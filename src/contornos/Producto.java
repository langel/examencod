/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package contornos;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author Luis
 */
public class Producto {
    
    /**
     * atributos
     */
    String nombre;
    float precio;
    int clave;
    Map <String,Producto> almacen = new TreeMap();

    public Producto() {
    }

    public Producto(String nombre, float precio) {
        this.nombre = nombre;
        this.precio = precio;
    }
    
    /**
     * método que recibe un objeto y lo añade a nuestro Map
     * @param producto1 que se le pasa desde el cuerpo principal
     */
    public void añade(Producto producto1){
        Scanner dato = new Scanner(System.in);
        System.out.println("Introduce nombre: ");
        nombre= dato.next();
        System.out.println("Introduce precio: ");
        precio=dato.nextFloat();
        System.out.println("Introduce un código: ");
        clave=dato.nextInt();
        almacen.put(String.valueOf(clave), producto1);
    }
   /**
    *  método para visualizar los objetos del Map
    */
    public void visualizar(){
        for(Map.Entry entrada:almacen.entrySet()){
            System.out.println("Producto: "+entrada.getKey()+","+entrada.getValue());
        }
    }

    /**
     * metodo que devuelve la variable nombre
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * metodo que da valor a la variable nombre
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo que devuelve la variable precio
     * @return the precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * metodo que da valor a la variable precio
     * @param precio the precio to set
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    
    
    
    
    
    
    
}
